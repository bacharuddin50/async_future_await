import 'package:soal3/soal3.dart' as soal3;

void main(List<String> arguments) async {
  print("Ready, Sing");
  print(await line());
  print(await line2());
  print(await line3());
  print(await line4());
}

Future<String> line() async {
  String lyric = "pernahkah kau merasa";
  return await Future.delayed(Duration(seconds: 5), () => (lyric));
}

Future<String> line2() async {
  String lyric = "pernahkah kau merasa.....";
  return await Future.delayed(Duration(seconds: 3), () => (lyric));
}

Future<String> line3() async {
  String lyric = "pernahkah kau merasa";
  return await Future.delayed(Duration(seconds: 2), () => (lyric));
}

Future<String> line4() async {
  String lyric = "Hatimu hampa, pernahkah kau merasa hati mu kosong....";
  return await Future.delayed(Duration(seconds: 1), () => (lyric));
}
